class user{
    _id;
    _name;
    _position;
    _office;
    _age;
    startDate;
    constructor(id,name,position,office,age,startDate){
        this._id = id;
        this._name = name;
        this._position = position;
        this._office = office;
        this._age = age;
        this.startDate = startDate;
    }
}
const user1 = new user( 1,"Airi Satou"," Accountant", "Tokyo", 33," 2008/11/28" );
const user2 = new user( 2,"Angelica Ramos","Chief Executive Officer(CEO)", "London",47,"2009/10/09");
const user3 = new user( 3,"Ashton Cox","Junior Technical Author", "San Francisco",66,"2009/01/12");
const user4 = new user(4,"Bradley Greer",
"Software Engineer",
"London",
41,
"2012/10/13");

const user5 = new user( 5,
    "Brenden Wagner",
    "Software Engineer",
   " San Francisco",
    28,
    "2011/06/07",
    );
const user6 = new user( 6,
    "Brielle Williamson",
    "Integration Specialist",
    "New York",
    61,
    "2012/12/02"
    );
const user7 = new user( 7,
    "Bruno Nash",
    "Software Engineer",
    "London",
    38,
    "2011/05/03"
    );
const user8 = new user( 8,
   " Caesar Vance",
    "Pre-Sales Support",
    "New York",
    21,
    "2011/12/12"
    );
const user9 = new user( 9,
    "Cara Stevens",
    "Sales Assistant",
    "New York",
    46,
    "2011/12/06"
    );
const user10 = new user( 10,
    "Cedric Kelly",
    "Senior Javascript Developer",
   " Edinburgh",
    22,
    "2012/03/29"
    );
const ListUser = new Array();
ListUser.push(user1,user2,user3,user4,user5,user6,user7,user8,user9,user10);

module.exports = {
    
    ListUser
}