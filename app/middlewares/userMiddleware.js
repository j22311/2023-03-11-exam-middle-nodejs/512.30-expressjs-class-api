const getAllUserMiddleware = (request,response,next) => {
   
   console.log('get all user (middleware)');
   next();
}

const getAnUserMiddleware = (request,response,next) => {
   
    console.log('get an user (middleware)');
    next();
 }

 const postAnUserMiddleware = (request,response,next) => {
   
    console.log('post an user (middleware)');
    next();
 }

const  putAnUserMiddleware = (request,response,next) => {
   
    console.log('put An user (middleware)');
    next();
 }

const deleteAnUserMiddleware = (request,response,next) => {
   
    console.log('delete an user (middleware)');
    next();
 }

 module.exports = {
    deleteAnUserMiddleware,
    putAnUserMiddleware,
    postAnUserMiddleware,
    getAnUserMiddleware,
    getAllUserMiddleware
 }