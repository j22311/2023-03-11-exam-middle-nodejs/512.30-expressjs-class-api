// khai báo thư viện 

const express = require("express");
const { ListUser } = require("../../user");
//import class user

const { getAllUserMiddleware, getAnUserMiddleware } = require("../middlewares/userMiddleware");

//tạo (khai báo ROuter)
const userRouter = express.Router();

userRouter.get('/users/',getAllUserMiddleware, (request,response) => {
    const age = request.body.age;
    console.log(age);
    if(!age || age == undefined){
        return response.json({
            message: 'get All users',
            data: ListUser
        })
    }
    function checkAge(Item){
        return Item > age;
    }
    const result = ListUser.filter((item)=>checkAge(item._age))
    return response.json({
        data: result
        
    })
})

userRouter.get('/users/:userId',getAnUserMiddleware,(request,response)=> {
    const userId = request.params.userId;
   
    if(isNaN(userId)){
        return response.status(400).json({
            message: 'bad Request',
            data:userId
        })
    }
    function checkId(Item){
        return Item == userId;
    }
    const result = ListUser.filter((item)=>checkId(item._id))
    return response.json({
        data: result
        
    })
})



module.exports = {
    userRouter
}