// khai báo thư viện express 
const express = require("express");
const { userRouter } = require("./app/routes/userRouter");

// khai báo app 
const app = express();

// Cấu hình request đọc được body json
app.use(express.json());

//khai báo port
const port = 8000;

app.listen(port, () => {
    console.log(`App listen on port  ${port}`)
})

app.use('/',userRouter)